#ifndef IPAPI_PROXY_H_INCLUDED
#define IPAPI_PROXY_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "co.ipapi.h"

extern const string IpapiPath;
extern const string IpapiName;

class IpapiProxy
: public DBusProxy,
  public co::ipapi_proxy
{
public:
    using ports_t = vector<uint16_t>;
    using relay_t = DBus::Struct<string, string, uint32_t, ports_t, ports_t>;
    using location_t = DBus::Struct<string, string, string, double, double, vector<relay_t>>;

    IpapiProxy(DBus::Connection &connection);
    virtual ~IpapiProxy() {}
};

#endif /* IPAPI_PROXY_H_INCLUDED */
