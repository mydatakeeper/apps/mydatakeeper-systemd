#include <iostream>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/download.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>
#include <mydatakeeper/application.h>
#include <mydatakeeper/systemd.h>

#include "ipapi-proxy.h"

using namespace std;

DBus::BusDispatcher dispatcher;

void _update(
    auto& system,
    const auto& name,
    const auto& fallback,
    auto get_auto_value,
    auto set_value,
    auto update_options)
{
    DBus::Variant v = system.Get(fr::mydatakeeper::ApplicationInterface, "fields");
    map<string,map<string,DBus::Variant>> fields = v;
    DBus::Variant& options = fields[name]["options"];

    cout << "Updating system " << name << endl;
    string value = system.Get(fr::mydatakeeper::ConfigInterface, name);

    if (value == "_auto") {
        clog << "Auto detect " << name << " value" << endl;
        value = get_auto_value();
        if (value.empty()) {
            clog << "Auto detect failed. Using fallback value " << fallback << endl;
            value = fallback;
        }
        options = update_options(&value, options);
    } else {
        options = update_options((string*)NULL, options);
    }
    cout << "Setting " << name << " to " << value << endl;
    set_value(value);

    clog << "Setting fields description" << endl;
    fields[name]["options"] = options;
    system.Set(fr::mydatakeeper::ApplicationInterface, "fields", to_variant(fields));
}

void update_locale(auto& system, auto& ipapi, auto& localed)
{
    try {
        _update(
            system, "locale", "fr_FR",
            [&ipapi]() {
                string languages = ipapi.languages();
                if (languages.empty())
                    return string();
                string locale = string(languages, 0, languages.find(','));
                if (locale[2] == '-') locale[2] = '_';
                if (locale[3] == '-') locale[3] = '_';
                return locale;
            },
            [&localed](auto& l) {
                vector<string> locales = {
                    "LANG=" + l,
                    "LANGUAGE=" + string(l, 0, l.find('_'))
                };
                localed.SetLocale(locales, false);
            },
            [](auto* v, auto& opts) {
                map<string, string> options = opts;
                if (v) {
                    options["_auto"] = string("Automatique (") + options[*v] + ')';
                    return to_variant(options);
                }
                if (options["_auto"] != "Automatique") {
                    options["_auto"] = "Automatique";
                    return to_variant(options);
                }
                return to_variant(options);
            }
        );
    } catch (DBus::Error& e) {
        cerr << "An error occured during system update : " << e.message() << endl;
    }
}

void update_timezone(auto& system, auto& ipapi, auto& timedated)
{
    try {
        _update(
            system, "timezone", "Europe/Paris",
            [&ipapi]() { return ipapi.timezone(); },
            [&timedated](auto& t) { timedated.SetTimezone(t, false); },
            [](auto* v, auto& opts) {
                map<string, DBus::Variant> options = opts;
                if (v) {
                    string str;
                    if (options.find(*v) != options.end()) {
                        str = options[*v].operator string();
                    } else {
                        auto idx = v->find('/');
                        string category(*v, 0, idx);
                        str = options[category].operator map<string, string>()[*v];
                    }
                    options["_auto"] = to_variant(string("Automatique (") + str + ')');
                }
                else {
                    DBus::Variant& var = options["_auto"];
                    string value = var.operator string();
                    if (value != "Automatique") {
                        options["_auto"] = to_variant(string("Automatique"));
                    }
                }
                return to_variant(options);
            }
        );
    } catch (DBus::Error& e) {
        cerr << "An error occured during system update : " << e.message() << endl;
    }
}

string admin_password_default;
void update_default_admin_password(auto& system)
{
    try {
        DBus::Variant v = system.Get(fr::mydatakeeper::ApplicationInterface, "fields");
        map<string,map<string,DBus::Variant>> fields = v;

        // Update default admin password
        fields["hash"]["default"] = to_variant(admin_password_default);

        system.Set(fr::mydatakeeper::ApplicationInterface, "fields", to_variant(fields));
    } catch (DBus::Error& e) {
        cerr << "An error occured during system default admin password update : " << e.message() << endl;
    }
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, script_dir;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/systemd", nullptr,
            "The Dbus path which handles the configuration"
        },
        {
            script_dir, "script-dir", 0, mydatakeeper::required_argument, "/usr/lib/mydatakeeper-systemd", nullptr,
            "The folder where configuration files will be generated"
        },
    });

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    string err;
    if (execute(script_dir + "/default-admin-password", {}, &admin_password_default, &err) != 0) {
        cerr << script_dir << "/default-admin-password: " << err << endl;
        return -1;
    }
    clog << "Configuring system with "
         << "admin password: " << admin_password_default << endl;


    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy system(conn, bus_path, bus_name);

    clog << "Getting Ipapi Proxy" << endl;
    IpapiProxy ipapi(conn);

    clog << "Getting Localed proxy" << endl;
    org::freedesktop::locale1Proxy localed(conn);

    clog << "Getting Timedated proxy" << endl;
    org::freedesktop::timedate1Proxy timedated(conn);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    system.onPropertiesChanged = [&system, &ipapi, &localed, &timedated](auto& iface, auto& changed, auto& invalidated)
    {
        if (changed.find("locale") != changed.end()) {
            clog << "Config locale has been updated" << endl;
            update_locale(system, ipapi, localed);
        }

        if (changed.find("timezone") != changed.end()) {
            clog << "Config timezone has been updated" << endl;
            update_timezone(system, ipapi, timedated);
        }
    };

    clog << "Setting up Ipapi update listener" << endl;
    ipapi.onPropertiesChanged = [&system, &ipapi, &localed, &timedated](auto& iface, auto& changed, auto& invalidated)
    {
        if (changed.find("languages") != changed.end()) {
            clog << "Ipapi languages has been updated" << endl;
            update_locale(system, ipapi, localed);
        }

        if (changed.find("timezone") != changed.end()) {
            clog << "Ipapi timezone has been updated" << endl;
            update_timezone(system, ipapi, timedated);
        }
    };

    // Force update during init
    update_default_admin_password(system);
    update_locale(system, ipapi, localed);
    update_timezone(system, ipapi, timedated);

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}