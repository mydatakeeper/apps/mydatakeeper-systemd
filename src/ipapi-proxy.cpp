#include "ipapi-proxy.h"

extern const string IpapiPath("/co/ipapi");
extern const string IpapiName("co.ipapi");

IpapiProxy::IpapiProxy(DBus::Connection &connection)
: DBusProxy(connection, IpapiPath, IpapiName)
{}

